/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
  /* 8 normal colors */
  "#1c1f24",  /* black   */
  "#ff6c6b",  /* red     */
  "#98be65",  /* green   */
  "#ECBE7B",  /* yellow  */
  "#2257A0",  /* blue    */
  "#a9a1e1",  /* magenta */
  "#5699AF",  /* cyan    */
  "#9ca0a4",  /* white   */

  /* 8 bright colors */
  "#3f444a",  /* black   */
  "#da8548",  /* red     */
  "#4db5bd",  /* green   */
  "#ECBE7B",  /* yellow  */
  "#51afef",  /* blue    */
  "#c678dd",  /* magenta */
  "#46D9FF",  /* cyan    */
  "#DFDFDF",  /* white   */

  [255] = 0,

  /* more colors can be added after 255 to use with DefaultXX */
  "#bbc2cf",
  "#5B6268",
  "#21242b",
};
